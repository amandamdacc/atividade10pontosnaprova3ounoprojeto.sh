#!/bin/bash

gerar_senha() {
    local comprimento=$1
    local caracteres=$2
    local senha=""
    local tamanho_caracteres=${#caracteres}

    for ((i=0; i<comprimento; i++)); do 
       local indice_aleatorio=$((RANDOM % tamanho_caracteres))
       senha+=${caracteres:indice_aleatorio:1}
    done

    echo "$senha"
}

yad --form --title "Gerador de Senhas" --width 400 --height 200 \
    --field="Tamanho da senha:NUM" \
    --field="Letras maiúsculas:CHK" \
    --field="Letras minúsculas:CHK" \
    --field="Números:CHK" \ 
    --field="Caracteres especiais:CHK" \
    --buttons-layout=center \
    --button="Gerar:0" \
    --button="Cancelar:1" \
    2>/dev/null | {
        read -r tamanho maiusculas minusculas numeros especiais botao

        if [[ $botao -eq 0 ]]; then 
            caracteres=""

            if [[ $maiusculas == "TRUE" ]]; then 
                caracteres+="ABCDEFGHIJKLMNOPQRSTUVXWYZ"
            fi 

            if [[ $minusculas == "TRUE" ]]; then 
                caracteres+="abcdefghijklmnopqrstuvxwyz"
            fi

            if [[ $numeros == "TRUE" ]]; then 
                caracteres+="0123456789"
            fi

            if [[ $especiais == "TRUE" ]]; then 
                caracteres+="~+=!@#$%&*"
            fi 

            if [[ -n $caracteres ]]; then 

                senha1=$(gerar_senha "$tamanho" "$caracteres")
                senha2=$(gerar_senha "$tamanho" "$caracteres")
                senha3=$(gerar_senha "$tamanho" "$caracteres")

                yad --title "Senhas geradas" --info --width 400 --text "Senhas geradas:
                Senha 1: $senha1
                Senha 2: $senha2
                Senha 3: $senha3"
            
            else 
                yad --title "Erro" --error --text "Selecione pelo menos uma opção de caracteres."
            fi
        fi
    }
